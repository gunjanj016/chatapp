const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.write("Hello  Gunjan");
  res.end("world");
});

app.get("/login", (req, res) => {
  res.write("Hello  login");
  res.end("PAge");
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});
